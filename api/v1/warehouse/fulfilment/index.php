<?php

/**
 * ProductDAO:
 * This Class is responsible for accessing Product data and performing
 * operations on Products.
 */
class ProductDAO
{
    // A HashMap of products to product ID for efficient searching.
    private $products;

    public function __construct($productList)
    {
        // Construct a Map of Products from a list of Products.
        $this->products = array();
        foreach ($productList as $product) {
            $this->products[$product->productId] = $product;
        }
    }

    /**
     * Input: An Integer Product ID.
     * Returns: Product object.
     * Description: Returns the Product of a specified Product ID.
     */
    public function getProduct($id)
    {
        return $this->products[$id];
    }

    // Added function for testing puroses only.
    public function getAllProducts()
    {
        return $this->products;
    }

    /**
     * Returns true if there are enough products to fulfil the specified Order.
     * Input: Order object.
     * Return: Boolean.
     */
    public function productsAvailable($order)
    {
        // For each item in the order
        foreach ($order->items as $item) {
            // Check that there is enough Product available to send
            $product = $this->getProduct($item->productId);
            if ($product->quantityOnHand < $item->quantity) {
                // There are not enough Products available to fill this Order.
                return false;
            }
        }
        // There are enough Products available to fill this Order.
        return true;
    }
}

/**
 * OrderDAO:
 * This class is responsible for accessing Order data.
 */
class OrderDAO
{
    // A HashMap of Order to Order ID for efficient searching.
    private $orders;
    // Provides an interfact to the Product data.
    private $productDAO;
    // An array of Purchase Orders.
    private $purchaseOrders = array();

    public function __construct($orderList, $productDAO)
    {
        $this->orders = array();
        foreach ($orderList as $order) {
            $this->orders[$order->orderId] = $order;
        }
        $this->productDAO = $productDAO;
    }

    /**
     * Input: An Integer Order ID.
     * Returns: Order object.
     * Description: Returns the Order of a specified Order ID.
     */
    public function getOrder($id)
    {
        return $this->orders[$id];
    }

    // Added function for testing puroses only.
    public function getPurchaseOrders()
    {
        return $this->purchaseOrders;
    }

    // Added function for testing puroses only.
    public function getAllOrders()
    {
        return $this->orders;
    }

    /**
     * Returns true if the Order was successfully fulfilled otherwise returns false.
     * Updates the Order status accordingly.
     */
    public function processOrder($order)
    {
        // Confirm that the Order can be fulfilled. No partially filled orders.
        if ($this->productDAO->productsAvailable($order)) {
            // Update the stock quantities and generate purchase orders to replenish low stock.
            foreach ($order->items as $item) {
                // Update stock quantities.
                $product = $this->productDAO->getProduct($item->productId);
                $product->quantityOnHand -= $item->quantity;

                // If the stock is falling below the threshold then order some more.
                // Don't generate another purchase order for a Product that is already being ordered.
                if ($product->quantityOnHand < $product->reorderThreshold && !in_array($product->productId, $this->purchaseOrders)) {
                    $this->purchaseOrders[] = $product->productId;
                }
            }
            $order->status = "Fulfilled";
            return true;
        }
        // Order was not fulfilled.
        $order->status = "Error: Unfulfillable";
        return false;
    }
}

/***************************
 * Application main body.
 ***************************/
// Accept an array of Order ID's.
$requestBody = json_decode(file_get_contents('php://input'));

// Read in data.json
$data = json_decode(file_get_contents('data.json'));

// Create Product instances.
$productDAO = new ProductDAO($data->products);
// Create Order instances.
$orderDAO = new OrderDAO($data->orders, $productDAO);

// web api response
$response = new stdClass();
// An array of order ids unfilfillable.
$response->unfulfillable = array();

// Process each of the requested Orders.
foreach ($requestBody->orderIds as $oid) {
    $order = $orderDAO->getOrder($oid);
    // If the order could not be processed then add it to the list of unfulfillables.
    if (!$order || !$orderDAO->processOrder($order)) {
        $response->unfulfillable[] = (int) $oid;
    }
}

print json_encode($response);

/**
 * Test code that is activated via a test input parameter.
 */
if ($requestBody->test == "true") {
    print "\n\n";

    print "Recieved request to process the following order ids:\n";
    print_r($requestBody->orderIds);

    print "\n\n";
    print "Generated the following purchase orders:\n";
    print_r($orderDAO->getPurchaseOrders());

    print "\n\n";
    print "Order data is as follows:\n";
    print_r($orderDAO->getAllOrders());

    print "\n\n";
    print "Product data is as follows:\n";
    print_r($productDAO->getAllProducts());

    print "\n";
}
